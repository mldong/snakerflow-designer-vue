import { GroupNode } from '@logicflow/extension'

class SubProcessView extends GroupNode.view {
}
class SubProcessModel extends GroupNode.model {
  initNodeData (data) {
    super.initNodeData(data)
    this.isRestrict = true
    this.resizable = true
    this.foldable = false
    this.width = 500
    this.height = 300
    this.foldedWidth = 50
    this.foldedHeight = 50
  }
}

const SubProcess = {
  type: 'snaker:subProcess',
  view: SubProcessView,
  model: SubProcessModel
}

export { SubProcess, SubProcessModel }
export default SubProcess
